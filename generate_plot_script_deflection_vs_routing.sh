#!/bin/bash
declare -a ktop_list=(1 2)
declare -a bs_list=(1 2 3 4 5)

for ktop in ${ktop_list[@]}
do
    cd $ktop
    mkdir output
    echo "cd" $ktop
    # remove wrong newlines
    for x in `ls n*_random*csv`; do cat $x | awk 'gsub(/\r/,"\n"){printf $0;next}{print}' > output/100-$x; done
    cd output
    # parse files
    for i in ${bs_list[@]}; do for x in `ls 100-*deflection_bs$i.csv`; do cat $x | tail -n +2 | awk '{r=$3;n=(1-r)*100;gsub(/.*/,n,$3);print $0}'; done |  sort -k3n > 200-random-deflection-bf$i.csv ; done
    for i in ${bs_list[@]}; do for x in `ls 100-*routing_bs$i.csv`; do cat $x | tail -n +2 | awk '{r=$3;n=(1-r)*100;gsub(/.*/,n,$3);print $0}'; done |  sort -k3n > 200-random-routing-bf$i.csv ; done

    ##########################

    # join results
    for i in ${bs_list[@]}
    do
      join -1 3 -2 3 200-random-deflection-bf$i.csv 200-random-routing-bf$i.csv  | awk 'BEGIN{print "traffic-load deflection routing"} {print $1" " $9" " $24}' > 300-drop-ratio-comparison-bf$i.csv
      join -1 3 -2 3 200-random-deflection-bf$i.csv 200-random-routing-bf$i.csv  | awk 'BEGIN{print "traffic-load deflection routing"} {print $1" " $12" " $27}' > 300-delay-comparison-bf$i.csv
      join -1 3 -2 3 200-random-deflection-bf$i.csv 200-random-routing-bf$i.csv  | awk 'BEGIN{print "traffic-load deflection routing"} {print $1" " $13" " $28}' > 300-delay-unmarked-comparison-bf$i.csv
      join -1 3 -2 3 200-random-deflection-bf$i.csv 200-random-routing-bf$i.csv  | awk 'BEGIN{print "traffic-load deflection routing"} {print $1" " $14" " $29}' > 300-delay-marked-comparison-bf$i.csv
      join -1 3 -2 3 200-random-deflection-bf$i.csv 200-random-routing-bf$i.csv  | awk 'BEGIN{print "traffic-load deflection routing"} {print $1" " $15" " $30}' > 300-received-unmarked-comparison-bf$i.csv
      join -1 3 -2 3 200-random-deflection-bf$i.csv 200-random-routing-bf$i.csv  | awk 'BEGIN{print "traffic-load deflection routing"} {print $1" " $16" " $31}' > 300-received-marked-comparison-bf$i.csv
    done
    
    for file in `ls 100-cdf*.csv`
    do
        #echo $file |  sed "s#_[^_]*_#_#" | sed "s#_[^_]*_#_#"
        mv $file `echo 400-$file |  sed "s#100-##" | sed "s#_[^_]*_#_#" | sed "s#_[^_]*_#_#"`
    done
   
    cd ..
    cd ..
    echo "cd .." 
done

mkdir output
# parse files
for b in ${bs_list[@]}
do 
    for p in ${ktop_list[@]}
    do
        for x in `ls $p/output/100-*-deflection*bs$b.csv`
        do
            cat $x | tail -n +2 | awk '{r=$3;n=(1-r)*100;gsub(/.*/,n,$3);print $0}' 
        done | sort -k3n > output/500-random-deflection-bf$b-p$p.csv 
    done
    for p in ${ktop_list[@]}
    do
        for x in `ls $p/output/100-*-routing*bs$b.csv`
        do
            cat $x | tail -n +2 | awk '{r=$3;n=(1-r)*100;gsub(/.*/,n,$3);print $0}' 
        done | sort -k3n > output/500-random-routing-bf$b-p$p.csv 
    done
done


ktop_list_para=""
bs_list_para=""
for ktop in ${ktop_list[@]}
do
  ktop_list_para=$ktop_list_para","$ktop
done
for i in ${bs_list[@]}
do
  bs_list_para=$bs_list_para","$i
done

python generate_plot_script_deflection_vs_routing.py $ktop_list_para $bs_list_para > commands.txt

gnuplot < commands.txt

mkdir graphs
mv *.png graphs/


