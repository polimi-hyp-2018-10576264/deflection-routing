#!/bin/bash

#for s in clos-no-deflection clos-deflection clos-upward-deflection clos-smart-deflection
for s in clos-buffer-false-deflection
do
    for b in `seq 1 1 10`
    do
        echo $s " " $b
        for i in `seq 0.05 0.05 0.95`
        do
            python simulator.py -r $i -l 3 -n clos -s $s -b $b &
        done
        python simulator.py -r 0 -l 3 -n clos -s $s -b $b 
    done
done 
