from distutils.core import setup, Extension

module1 = Extension('_ksptmodule',
                    sources = ['ksptmodule_wrap.cxx', 'ksptmodule.cpp'])

setup (name = 'ksptmodule',
       version = '1.0',
       description = 'This is a ksptmodule package',
       ext_modules = [module1],
       py_modules = ["ksptmodule"],
       ) 
