from distutils.core import setup, Extension

module1 = Extension('_kspmodule',
                    sources = ['kspmodule_wrap.cxx', 'kspmodule.cpp', 'Graph.cpp', 'DijkstraShortestPathAlg.cpp', 'YenTopKShortestPathsAlg.cpp'])

setup (name = 'kspmodule',
       version = '1.0',
       description = 'This is a kspmodule package',
       ext_modules = [module1],
       py_modules = ["kspmodule"],
       ) 
