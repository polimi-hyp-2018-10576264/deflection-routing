import random
import model.statistics as stat
import model.packet as packetmod


''' This class models a generator of packets for "speaker" nodes '''
''' It generates 'degree' packets at each vertex'''
class TrafficGeneratorMoreTraffic:
    
    ''' initialize a traffic generator for a set of 'nodes' nodes such
        that at most 'maxNumberPacketsPerNode' are generated to a specific node
        and a fraction 'limitInputRate' of the packets are directed to the 
        originator itself '''
    def __init__(self, speakers, maxNumberPacketsPerNode=None, limitInputRate=0):
        
        self.speakers = speakers
        self.maxNumberPacketsPerNode = maxNumberPacketsPerNode
        if maxNumberPacketsPerNode == None:
            self.maxNumberPacketsPerNode = len(speakers)
        self.limitInputRate = limitInputRate
                
    ''' returns a list containing a packet originated by each speaker.
        the set of packets is generated randomly '''
    def generateRandomClosTraffic(self,limit=False):
        if not limit:
            result = map(lambda x : packetmod.Packet(random.choice(self.nodes[:x]+self.nodes[(x+1):])),range(0,len(self.nodes)))
        else:
            #packets=map(lambda x : packet.Packet(x),self.nodes)
            result=[]
            for s in self.nodes:
                for i in range(0,self.maxNumberPacketsPerNode):
                    result.append(packetmod.Packet(s)) 
            #temp=[[p]*self.maxNumberPacketsPerNode for p in packets]
            #result=[row[index]  for row in temp for index in range(0,len(row))]
            random.shuffle(result)
            
            for packet in result:
                packet.setCreationTime(stat.Statistics().getIteration())
            
            #print "result",result
            
            # attemp to produce maximal throughput
            #print "result",result
            autoSendersIndices = filter(lambda x : self.nodes[x/self.maxNumberPacketsPerNode].__eq__(result[x].getDestination()),range(0,len(result)))
            ##print "autosendersIndices",autoSendersIndices
            while len(autoSendersIndices) > 0:
                #print "result",result
                #print "result",result
                autoSendersValuesSet = set(map(lambda x : result[x].getDestination(),autoSendersIndices))
                #print "autosendersIndices",autoSendersIndices
                #print "autosendersValues",autoSendersValuesSet
                if len(autoSendersValuesSet) == 1:
                    node=autoSendersValuesSet.pop()
                    nodePosition=node.getPosition()
                    for i in autoSendersIndices:
                        randomIndex = int(random.random()*len(result))
                        while randomIndex/self.maxNumberPacketsPerNode == nodePosition and result[randomIndex].getDestination().getPosition() != nodePosition:
                            randomIndex = int(random.random()*len(result))
                        temp = result[i]
                        result[i]=result[randomIndex]
                        result[randomIndex]=temp
                else:
                    autoSendersIndicesShuffled=autoSendersIndices[:]
                    random.shuffle(autoSendersIndicesShuffled)
                    #print "autosendersIndicesShffle",autoSendersIndicesShuffled
                    tempResult=map(lambda x: result[autoSendersIndicesShuffled[x]],range(0,len(autoSendersIndices)))
                    for i in range(0,len(tempResult)):
                        result[autoSendersIndices[i]]=tempResult[i]
                
                #print "result",result
                autoSendersIndices = filter(lambda x : self.nodes[x/self.maxNumberPacketsPerNode].__eq__(result[x].getDestination()),range(0,len(result)))
            
            
        stat.Statistics().increaseNumberOfPacketsSentBy(len(result))
        
        ''' apply limitInputRate option '''
        noneSenders = int(self.limitInputRate * len(result))
        #print "noneSenders",noneSenders,"out of", len(result)
        if noneSenders > 0:
            #noneIndices = []
            indices = range(0,len(result))
            random.shuffle(indices)
            for i in range(0,noneSenders):
                result[indices[i]]=None
                #value = random.choice(indices)
                #noneIndices.append(value)
                #indices.remove(value)
            #for index in noneIndices:
                #result[index]=self.nodes[index]
                #result[index]=None
        #print "result", result
        
        stat.Statistics().decreaseNumberOfPacketsSentBy(noneSenders)
        
        #remove packet where destination = source
        for i in range(0,len(result)):
            if result[i]!=None and result[i].getDestination().__eq__(self.nodes[i/self.maxNumberPacketsPerNode]):
                result[i]=None
                stat.Statistics().decreaseNumberOfPacketsSentBy(1)
        
        #populate buffers        
        for i in range(0,len(result)):
            if result[i] != None:
                self.nodes[i/self.maxNumberPacketsPerNode].addPacketIntoIngressBuffer(result[i])
                
        #print "nresult",result 
                 
        return result
                        
    id=0            
    ''' returns a list containing a packet originated by each speaker.
        the set of packets is generated randomly '''
    def generateRandomTraffic(self,limit=False,iteration=None):
        for node in self.speakers:
            for _ in node.getPorts():
                randomNumber = random.random()
                if randomNumber< 1-self.limitInputRate:
                    randomDestination = node
                    while randomDestination.__eq__(node):
                        randomDestination = random.choice(self.speakers)
                    #randomDestination=self.speakers[(8*i+3*j+1) % len(self.speakers)]
                    packet = packetmod.Packet(node, randomDestination)
                    #packet.setID(TrafficGeneratorMoreTraffic.id)                    
                    packet.setCreationTime(stat.Statistics().iteration)
                    #print "stattrafiteration:", stat.Statistics().iteration
                    node.addPacketIntoIngressBuffer(packet)
                    stat.Statistics().increaseNumberOfPacketsSentBy(1)
                    #TrafficGeneratorMoreTraffic.id+=1
                
                
        
            