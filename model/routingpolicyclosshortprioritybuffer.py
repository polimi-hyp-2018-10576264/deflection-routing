
import model.routingpolicy as rp
import random 
import model.statistics as stat
from collections import deque

'''
    This class implements a Routing Policy for a Switch in a Clos network
    without ever sending a packet backwards. Each packet is sent upwards
    to a top level switch and then downwards to the destination. Observe that, 
    if each speaker generates a vertex, then deflecting a packet backwards 
    will result in a drop. 
    - all packets that must be sent upwards are randomly mapped to any upward
      neighbor.
    - all packets that must be sent downwards have a unique neighbor. Contention
      may happen and is resolved by dropping packets.
      
    add debug info by replacing '#print' with '#print'
'''
class RoutingPolicyClosShortcutPriorityBuffer(rp.RoutingPolicy):
    
    test=False
    
    def __init__(self,switch,type=True):
        rp.RoutingPolicy.__init__(self,switch)
        self.type=type
        
    ''' processes incoming packets and returns a map 'interface name' -> packet 
        it always sends a packet upward to a toplevel switch and then downward to the destination '''   
    def processPackets(self,packets):
        output = {}
        
        aboveEdges=self.switch.getEdgesAboveLevel()
        belowEdges=self.switch.getEdgesLowerLevel()
        freeAboveEdgesMarked=filter(lambda e : len(self.switch.getPorts()[e]) < self.switch.getBufferSize(),aboveEdges[:])
        freeBelowEdgesMarked=filter(lambda e : len(self.switch.getPorts()[e]) < self.switch.getBufferSize(),belowEdges[:])
        freeAboveEdgesUnmarked=filter(lambda e : len(filter(lambda p : p.isDeflected(),self.switch.getPorts()[e])) > 0 or len(self.switch.getPorts()[e]) < self.switch.getBufferSize(),aboveEdges[:])
        freeBelowEdgesUnmarked=filter(lambda e : len(filter(lambda p : p.isDeflected(),self.switch.getPorts()[e])) > 0 or len(self.switch.getPorts()[e]) < self.switch.getBufferSize(),belowEdges[:])
        
        
        degree= len(aboveEdges)
        if degree == 0:
            degree=len(belowEdges) 

            
        
        ##print "i'm",self.switch,"no top no speaker"
         
        #print "**i'm",self.switch, "with buffer", self.switch.getBufferSize()
        while len(self.switch.getIngress()) > 0:
            packet = self.switch.getIngress().popleft()
            if packet.getDestination().__eq__(self.switch):
                #packet arrived to destination
                ##print self.switch, "received packet for myself"
                stat.Statistics().recordSuccess(packet)
                ##print "getSuccess",stat.Statistics().getSuccess()
                #self.sendPacket(freeAboveEdges, output, packet)
            else: 
                self.sendPacket( freeBelowEdgesUnmarked, freeBelowEdgesMarked , freeAboveEdgesUnmarked, 
                                                           freeAboveEdgesMarked, output, packet,degree, belowEdges)
        
        packetsKeys= packets.keys()
        random.shuffle(packetsKeys)
         
        edgeWithDownwardDeflectedPackets =filter(lambda x : packets[x].isDeflected(),packetsKeys)
        edgeWithNonDownwardDeflectedPackets =filter(lambda x : not packets[x].isDeflected(),packetsKeys)
                 

        ##print "i'm", self.switch
        #for (a,n) in allEdges:
        for edge in edgeWithNonDownwardDeflectedPackets:
            ##print "edge", (a,n)
            #if len(n.getPorts()[(a,self.switch)]) > 0:
            ##print "pkts", n.getPorts()[(a,self.switch)]
            packet =packets[edge]
            #print " packet to",packet, "from", edge
            if packet.getDestination().__eq__(self.switch):
                #packet arrived to destination
                ##print self.switch, "received packet for myself"
                stat.Statistics().recordSuccess(packet)
                #self.sendPacket(freeAboveEdges, output, packet)
                continue
            sent=False
            
            isDirectedDownward = self.isPacketDirectedDownward(degree, packet.getDestination(), belowEdges)
            sent=False
            if isDirectedDownward:
                sent=self.sendDownward(degree, freeBelowEdgesUnmarked, freeBelowEdgesMarked , freeAboveEdgesUnmarked, 
                                       freeAboveEdgesMarked, output, packet, belowEdges)  
                if not sent:
                    packet.markAsDeflected()
                    edgeWithDownwardDeflectedPackets.append(edge)
                    continue                  
            if not sent or not isDirectedDownward :
                if not self.sendPacket( freeBelowEdgesUnmarked, freeBelowEdgesMarked , freeAboveEdgesUnmarked, 
                                                                  freeAboveEdgesMarked,  output, packet,degree,belowEdges):
                    packet.markAsDeflected()
                    edgeWithDownwardDeflectedPackets.append(edge)
                    continue

                          
        for edge in edgeWithDownwardDeflectedPackets:
            ##print "edge", (a,n)
            #if len(n.getPorts()[(a,self.switch)]) > 0:
            ##print "pkts", n.getPorts()[(a,self.switch)]
            #stat.Statistics().increaseDropByOne()
            #return {}
            packet =packets[edge]
            #stat.Statistics().increaseDropByOne()
            #continue
            ##print "packet to",packet, "from", edge
            ##print "hoy"
            self.sendDeflectedPacket(packet,edge,degree, freeBelowEdgesUnmarked, freeBelowEdgesMarked , freeAboveEdgesUnmarked, 
                                     freeAboveEdgesMarked,  output,belowEdges)
           
        ##print "ports", self.switch.getPorts()                                   
        return output
        
        
    def sendDeflectedPacket(self,packet,edge,degree,  freeBelowEdgesUnmarked, freeBelowEdgesMarked , freeAboveEdgesUnmarked, 
                            freeAboveEdgesMarked, output,belowEdges):
        #stat.Statistics().increaseDropByOne()
        #continue
        if packet.getDestination().__eq__(self.switch):
            #packet arrived to destination
            ##print self.switch, "received packet for myself"
            stat.Statistics().recordSuccess(packet)
            #self.sendPacket(freeAboveEdges, output, packet)
            return
        sent=False
        if self.isPacketDirectedDownward(degree, packet.getDestination(), belowEdges):
            sent=self.sendDownward(degree,freeBelowEdgesUnmarked, freeBelowEdgesMarked , freeAboveEdgesUnmarked, 
                                   freeAboveEdgesMarked,   output, packet, belowEdges) 
        if not sent:
            if not self.sendPacket(freeBelowEdgesUnmarked, freeBelowEdgesMarked , freeAboveEdgesUnmarked, 
                                                             freeAboveEdgesMarked,  output, packet,degree,belowEdges):
                #if self.isPacketDirectedDownward(degree, packet, belowEdges): 
                ##print "************* DEFLECTION!! packet",packet,"at",self.switch
                if not self.sendRandomDownward(freeBelowEdgesUnmarked,freeBelowEdgesMarked, output, packet):
                    ##print "************* HERE!!", freeBelowEdgesUnmarked,freeBelowEdgesMarked,self.switch,packet,output
                    stat.Statistics().increaseDropByOne()
                    ##print "i'm", self.switch, "available", freeAboveEdges,"edge",edge
                    
        #print "finish rerouting"
            
        
    
    def isPacketDirectedDownward(self,degree,destination,belowEdges):
        level = self.switch.getLevel()
        position = self.switch.getPosition() 
        widthPod = degree**(level)
        ##print "level",level,"position",position,"widthPod",widthPod, "destination",destination,"degree",degree, "belowEdges",belowEdges
        pod = position / widthPod
        return pod == destination.position/widthPod
        #return len(filter(lambda (a,n): (n.position-(pod*widthPod))/widthSubPod== (destination.position-(pod*widthPod))/widthSubPod,belowEdges))!=0
     
    ''' it tries to send a packet upward to a random available edge. If it does not exists, it fails 
        but it does not drop the packet'''
    def sendPacket(self, freeBelowEdgesUnmarked, freeBelowEdgesMarked , freeAboveEdgesUnmarked, 
                                             freeAboveEdgesMarked, output,packet,degree, belowEdges):
        if packet.isDeflected():
            freeAboveEdges=freeAboveEdgesMarked
            if len(freeAboveEdges) > 0:
                if self.type:
                    edge = self.getLeastLoadedEdge(freeAboveEdges,output)
                else:
                    edge =random.choice(freeAboveEdges)
            #print "1freeAbove", freeAboveEdges
            #print "          ", freeAboveEdgesUnmarked
            #print "          ", freeAboveEdgesMarked
        else:
            freeAboveEdges=freeAboveEdgesUnmarked
            #if self.type:
                #if len(freeAboveEdgesMarked) >0:
                #    freeAboveEdges=freeAboveEdgesMarked
                #else:
                #    freeAboveEdges=freeAboveEdgesUnmarked
            #else:
            if len(freeAboveEdges):
                edge =random.choice(freeAboveEdges)
                #print "1freeAbove", freeAboveEdges
                #print "          ", freeAboveEdgesUnmarked
                #print "          ", freeAboveEdgesMarked
        
        if len(freeAboveEdges) >0:
            #print "2freeAbove", freeAboveEdges
            #print "          ", freeAboveEdgesUnmarked
            #print "          ", freeAboveEdgesMarked
            
            #edge =freeAboveEdges[0]
            edge =random.choice(freeAboveEdges)
            ##print "keys",output.keys()
            if not edge in output.keys():
                ##print "add key",edge,"to output"
                output[edge]=deque([])
            #print "plan to send packet",packet,"to",edge
            #print "  buffer",self.switch.getPorts()[edge]
            #print "  output",output[edge]
            ##print "free", freeAboveEdges, "ports",self.switch.getPorts().keys()
            if len(self.switch.getPorts()[edge]) +1 +len(output[edge])== self.switch.getBufferSize():
                ##print "remove", edge 
                #print "3freeAbove", freeAboveEdges
                #print "          ", freeAboveEdgesUnmarked
                #print "          ", freeAboveEdgesMarked
                freeAboveEdgesMarked.remove(edge)
                #print "4freeAbove", freeAboveEdges
                #print "          ", freeAboveEdgesUnmarked
                #print "          ", freeAboveEdgesMarked
                pd = filter(lambda p : p.isDeflected(),self.switch.getPorts()[edge])
                pdOutput = filter(lambda p : p.isDeflected(),output[edge])
                lenpd = len(pd) + len(pdOutput)
                if lenpd == 0 and not packet.isDeflected():
                    freeAboveEdgesUnmarked.remove(edge)
            
            if not packet.isDeflected() and len(self.switch.getPorts()[edge]) +1 +len(output[edge])> self.switch.getBufferSize():        
                pd = filter(lambda p : p.isDeflected(),self.switch.getPorts()[edge])
                lenpd = len(pd)
                if len(pd)==0:
                    pd = filter(lambda p : p.isDeflected(),output[edge])
                pck = pd[0]
                ##print "buffer", self.switch.getPorts()[edge]
                ##print "output", output[edge]
                ##print "removed packet", pck
                #print "rerouting",pck
                if pck in self.switch.getPorts()[edge]:
                    self.switch.getPorts()[edge].remove(pck)
                else:
                    output[edge].remove(pck)
                self.sendDeflectedPacket(pck,edge,degree,  freeBelowEdgesUnmarked, freeBelowEdgesMarked , freeAboveEdgesUnmarked,
                                         freeAboveEdgesMarked, output,belowEdges)
                if len(pd) == 1:
                    #print "5freeAbove", freeAboveEdges
                    #print "          ", freeAboveEdgesUnmarked
                    #print "          ", freeAboveEdgesMarked
                    freeAboveEdgesUnmarked.remove(edge)
                ##print "buffer", self.switch.getPorts()[edge]
                    
            ##print "output", output[edge]
            output[edge].append(packet)
            #print "send packet",packet,"to",edge
            #print "  buffer",self.switch.getPorts()[edge]
            #print "  output",output[edge]
            #print "----"
            ##print "output", output[edge]
            return True
        else:
            packet.markAsDeflected()
            return False
            
    def sendRandomDownward(self,freeBelowEdgesUnmarked,freeBelowEdgesMarked,output,packet):
        if len(freeBelowEdgesMarked) >0:
            #edge =freeBelowEdgesMarked[0]
            edge =random.choice(freeBelowEdgesMarked)
            freeBelowEdges=freeBelowEdgesMarked
            if len(freeBelowEdges) > 0:
                if self.type:
                    edge = self.getLeastLoadedEdge(freeBelowEdges,output)
                else:
                    edge =random.choice(freeBelowEdges)
            #print "8freeBelow", freeBelowEdgesUnmarked
            #print "          ", freeBelowEdgesMarked
            if not edge in output.keys():
                output[edge]=deque([])
            if len(self.switch.getPorts()[edge]) +1 +len(output[edge])== self.switch.getBufferSize(): 
                freeBelowEdgesMarked.remove(edge)
            output[edge].append(packet)
            #print "9freeBelow", freeBelowEdgesUnmarked
            #print "          ", freeBelowEdgesMarked
            ##print "send packet",packet,"to",edge
            return True
        else:
            packet.markAsDeflected()
            ##print "packet",packet,"is marked as deflected"
            return False
    
    def sendDownward(self,degree, freeBelowEdgesUnmarked, freeBelowEdgesMarked , freeAboveEdgesUnmarked, 
                     freeAboveEdgesMarked, output,packet,belowEdges):
        if packet.isDeflected():
            freeBelowEdges=freeBelowEdgesMarked
        else:
            freeBelowEdges=freeBelowEdgesUnmarked
        
        if len(freeBelowEdges)>0:
            ##print "i'm", self.switch, "trying to send packet", packet
            level = self.switch.getLevel()
            position = self.switch.getPosition() 
            widthPod = degree**(level)
            ##print "level",level,"position",position,"widthPod",widthPod, "packet",packet,"degree",degree
            pod = position / widthPod
            widthSubPod = widthPod/degree
            #for (a,n) in freeBelowEdges:
            #    #print "op1",(int(n.name.split('_')[1])-(pod*widthPod))/widthSubPod,"op2",(int(packet.name.split('_')[1])-(pod*widthPod))/widthSubPod                              
            edges = filter(lambda (a,n): (n.position-(pod*widthPod))/widthSubPod== (packet.getDestination().position-(pod*widthPod))/widthSubPod,freeBelowEdges)
            if len(edges) == 0:
                packet.markAsDeflected()
                ##print "packet",packet,"is marked as deflected"
                ##print "##########output",output
                return False
            else:
                edge = edges[0]
                if not edge in output.keys():
                    output[edge]=deque([])
                #print "plan to send packet",packet,"to",edge
                #print "  buffer",self.switch.getPorts()[edge]
                #print "  output",output[edge]
                #if len(self.switch.getPorts()[edge]) > 0:
                #    if self.switch.getPorts()[edge][0].isDeflected():
                #        if len(self.switch.getPorts()[edge])+len(output[edge]) >= self.switch.getBufferSize():
                #            #print "len",len(self.switch.getPorts()[edge]), "output", output[edge], "len", len(output[edge])
                
                if len(self.switch.getPorts()[edge]) +1 +len(output[edge])== self.switch.getBufferSize():
                    ##print "remove", edge 
                    if edge in freeBelowEdgesMarked:
                        freeBelowEdgesMarked.remove(edge)
                    pd = filter(lambda p : p.isDeflected(),self.switch.getPorts()[edge])
                    pdOutput = filter(lambda p : p.isDeflected(),output[edge])
                    lenpd = len(pd) + len(pdOutput)
                    if lenpd == 0 and not packet.isDeflected():
                        ##print "******remove"
                        freeBelowEdgesUnmarked.remove(edge)
                
                if not packet.isDeflected() and len(self.switch.getPorts()[edge]) +1 +len(output[edge])> self.switch.getBufferSize():        
                    pd = filter(lambda p : p.isDeflected(),self.switch.getPorts()[edge])
                    lenpd = len(pd)
                    if len(pd)==0:
                        pd = filter(lambda p : p.isDeflected(),output[edge])
                    pck = pd[0]
                    ##print "buffer", self.switch.getPorts()[edge]
                    ##print "output", output[edge]
                    ##print "removed packet", pck
                    if pck in self.switch.getPorts()[edge]:
                        self.switch.getPorts()[edge].remove(pck)
                    else:
                        output[edge].remove(pck)
                    #print "rerouting",pck
                    self.sendDeflectedPacket(pck,edge,degree,  freeBelowEdgesUnmarked, freeBelowEdgesMarked , freeAboveEdgesUnmarked,
                                             freeAboveEdgesMarked, output,belowEdges)
                    #print " pd",pd
                    if len(pd) == 1:
                        freeBelowEdgesUnmarked.remove(edge)
                        #print " removed",edge
                    #print " freeBelow",freeBelowEdgesUnmarked

                output[edge].append(packet)
                #print "send packet",packet,"to",edge
                #print "  buffer",self.switch.getPorts()[edge]
                #print "  output",output[edge]
                #print "----"
                ##print "packet",packet,"not marked as deflected",packet.isDeflected(), "output", output[edge], "---",output
                return True
        else:
            packet.markAsDeflected()
            ##print "packet",packet,"is marked as deflected"
            ##print "##########output",output
            return False
        
    def getLeastLoadedEdge(self,edges,output):
        min = None
        minValue = 10000000
        for edge in edges:
            outputValue=0
            if edge in output.keys():
                outputValue = len(output[edge])
            if len(self.switch.getPorts()[edge]) + outputValue < minValue:
                min=edge
                minValue = len(self.switch.getPorts()[edge]) +outputValue
        return min

            
            
            