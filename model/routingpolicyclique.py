
import routingpolicy as rp

'''
    This class implements a Routing Policy for a Switch in a Clique network.
    - each packet is directly send to the destination vertex.
'''
class RoutingPolicyClique(rp.RoutingPolicy):
    
    def __init__(self,switch):
        rp.RoutingPolicy.__init__(self,switch)
                
    ''' processes incoming packets and returns a map (interfaceName,neighbor) -> packet '''    
    def processPackets(self):
        output = {}
        
        ''' processes packets originated by the switch itself'''
        if len(self.switch.getIngress()) > 0:
                packet = self.switch.getIngress().popleft()
                if packet.__eq__(self.switch):
                    #packet arrived to destination
                    pass
                    print self.switch, "received packet from myself"
                else:
                    print "packet",packet,"from ingress"
                    interfaceName = self.switch.getInterfaceTo(packet)
                    output[(interfaceName,packet)]=packet
        
        ''' processes packets received from neighbors '''
        for sbkey in self.switch.getPorts().keys():
            if len(self.switch.getPorts()[sbkey]) > 0:
                packet = self.switch.getPorts()[sbkey].popleft()
                if packet.__eq__(self.switch):
                    #packet arrived to destination
                    pass
                    print self.switch, "received packet from", sbkey[1], "through", sbkey[0]
                    
        return output
            
            