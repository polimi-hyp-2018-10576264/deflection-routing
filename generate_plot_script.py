
import sys 

print "set xrange[5:100]"
print "set yrange[0:100]"
print "set key left"
print "set term png"


xaxis = "traffic_load (%)"
print("set xlabel \""+xaxis+"\"")


measures = [
["drop-ratio","drop_ratio (%)","9","00","drop-ratio-comparison-bfSIZE.csv"],
["delay","delay (iterations)","12","01","delay-comparison-bfSIZE.csv"],
["delay-unmarked","delay-unmarked (iterations)","13","02","delay-unmarked-comparison-bfSIZE.csv"],
["delay-marked","delay-marked (iterations)","14","03","delay-marked-comparison-bfSIZE.csv"],
["received-unmarked","fraction of unmarked success (%)","15","04","received-unmarked-comparison-bfSIZE.csv"],
["received-marked","fraction of marked success (%)","16","05","received-marked-comparison-bfSIZE.csv"]
]

deflection_functions = [
["NAME-no-deflection.png","no-deflection vertex_degree=24","clos-no-deflection-bfSIZE.csv","01","2","no-deflection"],
["NAME-upward-deflection.png","upward-deflection vertex_degree=24","clos-upward-deflection-bfSIZE.csv","02","3","upward"],
["NAME-deflection.png","deflection vertex_degree=24","clos-deflection-bfSIZE.csv","03","4","deflection"],
["NAME-priority-buffer-false-deflection.png","priority-deflection vertex_degree=24","clos-buffer-false-deflection-bfSIZE.csv","04","5","priority"] ,
["NAME-priority-buffer-false-noreordering-deflection-.png","priority-deflection no-reordering vertex_degree=24","clos-buffer-false-deflection-bfSIZE-noreordering.csv","05","6","priority-no-reordering"] 
]
print ""

h=0
for measure in measures:
    print("set ylabel \""+measure[1]+"\"")
  
    j=0
    for deflection_function in deflection_functions:
        if h>1 and j<3:
            j+=1
            continue
        sys.stdout.write("set output \""+measure[3]+"_"+deflection_function[3]+"_"+deflection_function[0].replace("NAME",measure[0])+"\"\n")
        sys.stdout.write("set title \""+deflection_function[1]+"\"\n")
        print "plot ",
        for i in range(1,10):
            sys.stdout.write("\""+deflection_function[2].replace("SIZE",str(i))+"\" using 3:"+measure[2]+" with lines title \"bs "+str(i)+"\",\\\n")
        sys.stdout.write("\""+deflection_function[2].replace("SIZE","10")+"\" using 3:"+measure[2]+" with lines title \"bs 10\"\n")
        print ""
        j+=1
    h+=1
    
    
print ""
print ""
print ""
print "set xlabel \"traffic_load (%)\""
print "set style fill solid border rgb \"black\""
print "set auto x"
print "set yrange [0:100]"

h=0
for measure in measures:
    print("set ylabel \""+measure[1]+"\"")
  
    
    for i in range(1,11):
  
        sys.stdout.write("set output \"comp_"+measure[3]+"_"+str(i)+".png\"\n")
        sys.stdout.write("set title \"buffer_size="+str(i)+"\"\n")
        print "plot ",
        j=0
        for deflection_function in deflection_functions:
            if j< 4:
                sys.stdout.write("\""+measure[4].replace("SIZE",str(i))+"\" using 1:"+deflection_function[4]+" with lines title \""+deflection_function[5]+"\",\\\n")
            j+=1
        sys.stdout.write("\""+measure[4].replace("SIZE",str(i))+"\" using 1:"+deflection_functions[4][4]+" with lines title \""+deflection_functions[4][5]+"\"\n")
        print ""

    h+=1
    





