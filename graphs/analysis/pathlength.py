import sys
import collections

if len(sys.argv) < 2:
	print >> sys.stderr, "usage: %s files port" % (sys.argv[0])
vals = collections.defaultdict(lambda: 0)
total = 0 
with open(sys.argv[1], 'r') as f:
	data = f.readline()
	while data:
		total += 1
		try:
			val = int(data.split(';')[int(sys.argv[2])].split(',')[1])
			vals[val] += 1
		except:
			data = f.readline()
			continue
		data = f.readline()

so_far = 0
output = ""
minval = min(vals.keys())
maxval = max(vals.keys())
for k in range(0, minval):
	output += "%d,"%(0)
for k in sorted(vals.keys()):
	so_far += vals[k]
	output += "(%d %d),"%(k, vals[k])
for k in range(maxval+1, 6):
	output += "%d,"%(0)
output += "%d"%(total-so_far)
print output